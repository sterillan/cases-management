# Cases Management
## Getting started

## Name
Administrador de Casos, con la posiblidad de crear, actualizar y eliminar notas. 

## Description
Este proyecto ha sido desarrollado con JavaScript. Se utilizó la version 15.0.1 de NodeJS, con Express como framework para el backend. Mongoose para la creacion, consulta y administración de la base de datos MongoDB, que puede ser administrada de forma gráfica a través de Mongo Compass. 
El frontend fue desarrollado con ReactJS, utilizando TailwindCSS para el frontend. 


## Visuals
Frontend 

https://gitlab.com/sterillan/cases-management/-/raw/development/frontend/capure.png

## Installation
Este proyecto ha sido desarrollado bajo un entorno Linux Ubuntu. 

Para correr el proyecto localmente, es necesario clonar el repositorio y ejecutar los siguientes comandos:

```
git clone https://gitlab.com/sterillan/cases-management.git
cd backend
npm install
npm run dev 
cd ..
cd frontend
npm install
npm run dev
```

## Usage
Desde el navegador en localhost:3000 podrá visualizar e interactuar con la aplicación.

## Support
Para cualquier duda sobre el proyecto pueden contactarme en sterillan@gmail.com

## TODOs
Pendientes

- [ ] [Cargar en el formulario la información de la nota que se está editando](1)
- [ ] [Optimizar las consultas en la DB](2)
- [ ] [](3)


## Project status
Este proyecto no está terminado. Aún existen mejoras pendientes.
