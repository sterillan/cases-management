import Case from '../models/Case.js'

/* Events.find({ DevEui: DevEUI })
.sort({ $natural: -1 })
.limit(1)
.exec(function (err, results) {
  // La Siguiente Condicion Nos Mostrara en la Terminal el Mensaje de Error en Caso de que Ocurra Algun Error en la Consulta a la Collections Events.
  if (err) { Events.find({ DevEui: DevEUI })
  .sort({ $natural: -1 })
  .limit(1)
  .exec(function (err, results) {
    // La Siguiente Condicion Nos Mostrara en la Terminal el Mensaje de Error en Caso de que Ocurra Algun Error en la Consulta a la Collections Events.
    if (err) {
      console.log(err);
    }
    console.log(err);
  } */

export const getAllCases = async (req, res, next) => {

  console.log('All is good')
  Case.find()
  return('200 OK')
/*   try {
    const newCase = new Case(req.body);
    await newCase.save();
    res.redirect("/");
  } catch (error) {
    return res.render("error", { errorMessage: error.message });
  } */
};

export const createCase = async (req, res, next) => {
    try {
      const newCase = new Case(req.body);
      await newCase.save();
      res.redirect("/");
      console.log("New case created")
    } catch (error) {
      return "Error" /* res.render("error", { errorMessage: error.message }); */
    }
  };

  export const editCase = async (req, res, next) => {
    const { id } = req.params;
    await Case.updateOne({ _id: id }, req.body);
    res.redirect("/");
  };
  
  export const deleteCase = async (req, res, next) => {
    let { id } = req.params;
    await Case.remove({ _id: id });
    res.redirect("/");
  };