import pkg from 'mongoose';
const { Schema, model } = pkg;

const CaseSchema = Schema(
  {
    case_id: { type: Number, required: true, trim: true, unique: true },
    notes: {
      id: { type: Number, unique: true },
      note: { type: String, trim: true },
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Case", CaseSchema);

/* case id
notes
ide
note */
