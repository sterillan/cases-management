import pkg from 'mongoose';
const { connect } = pkg;
import { MONGODB_URI } from "../../config.js";

// connection to db
(async () => {
  try {
    const db = await connect(MONGODB_URI);
    console.log("Db connected to", db.connection.name);
  } catch (error) {
    console.error(error);
  }
})();