import { Router } from "express";
import { 
  getAllCases,
  createCase,
  editCase,
  deleteCase
} from "../controllers/case.controller.js";

const router = Router();

// Render all tasks
router.get("/", getAllCases);

router.post("/cases/add", createCase);

router.post("/cases/:id/edit", editCase);

router.get("/cases/:id/delete", deleteCase);

export default router;