import express from "express";
import morgan from "morgan";

import indexRoutes from "./src/routes/case.routes.js";


const app = express();

// settings
app.set("port", process.env.PORT || 5000);

// middlewares
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));

// routes
app.use(indexRoutes);

app.use((req, res, next) => {
  res.status(404).render("404");
});

export default app;