import { Link, Navigate } from "react-router-dom";
import Search from "../components/Search";
import Footer from "../components/Footer";
import Form from "../components/Form";

const Home = () => {
  return (
    <>
      
<div className="min-h-screen">
<div className="md:flex">
        <main className="flex-1 bg-transparent mx-20 mt-5 p-10 border rounded-xl h-fit md:w-2/3 overflow-auto  shadow-xl">
          <Search />
          <div className="w-full mb-3 grid place-content-center">
            <Form />
          </div>
        </main>
      </div>

      <Footer />
</div>
     
    </>
  );
};

export default Home;
