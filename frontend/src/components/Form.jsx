const Form = () => {
    return(
        <>
            <form>
    <span className='text-gray-900 line-leading-snug text-border text-xl text-center font-black block mb-3'>Crear nueva nota</span>

                <div className="mb-6">
                
                <input
                    type="text"
                    id="case"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 "
                    placeholder="ID case"
                    required=""
                />
                </div>

                <div className="mb-6">
                <label
                    for="password"
                    className="block mb-2 text-sm  text-gray-900 text-center font-semibold"
                >
                    Notas
                </label>
                <input
                    type="text"
                    id="note"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 "
                    required=""
                    placeholder="ID Note"
                />
                </div>
                <div class="mb-6">
                
                <input
                    type="text"
                    id="large-input"
                    placeholder="Descripcion...                 "
                    className="block p-4 w-full text-gray-900 bg-gray-50 rounded-lg border border-gray-300 sm:text-md focus:ring-blue-500 focus:border-blue-500 "
                />
                </div>

                <button
                type="submit"
                className="block w-full align-middle content-center text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm sm:w-auto px-7 py-1.5 ml-1 text-center"
                >
                Guardar
                </button>
            </form>
            <div>
                <button className="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg text-xs w-full sm:w-auto px-3 py-1.5 text-center m-1">Descargar JSON</button>
                <button className="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg text-xs w-full sm:w-auto px-3 py-1.5 text-center m-1">Tranferir FTP</button>
            </div>
        </>
    )
}

export default Form