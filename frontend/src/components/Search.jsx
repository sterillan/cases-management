const Search = () => {
  return (
    <>
      <input
        type="text"
        id="search-navbar"
        className="text-gray-900 bg-gray-50 rounded-lg border border-gray-300 sm:text-sm focus:ring-blue-500 focus:border-blue-500 m-1 px-3 py-1.5"
        placeholder="Case ID"
      />
      <button
        type="submit"
        className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-3 py-1.5 text-center"
      >
        Buscar
      </button>
    </>
  );
};

export default Search;
